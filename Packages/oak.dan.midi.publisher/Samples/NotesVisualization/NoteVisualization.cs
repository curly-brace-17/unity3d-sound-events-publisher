using System;
using UnityEngine;

namespace Oak.Dan.SoundEventsPublisher.NotesVisualization
{
    [RequireComponent(typeof(MeshFilter))]
    public class NoteVisualization : MonoBehaviour
    {
        // Required fields
        
            [SerializeField]
            private Single startTime;
            
            [SerializeField]
            private Single finishTime;

            [SerializeField] 
            private Single speed;
            
        public Boolean HasStarted => hasStarted;
        
        public Boolean HasFinished => hasFinished;

        public Mesh Mesh => meshFilter.mesh;

        public Single StartTime { get => startTime; set => startTime = value; }

        public Single FinishTime { get => finishTime; set => finishTime = value; }

        // Debug

            [SerializeField]
            private Boolean hasStarted;
            
            [SerializeField]
            private Boolean hasFinished;

            [SerializeField]
            private Single quadLength;

        private MeshFilter meshFilter;

        private Vector3 startPosition;
        
        public void SetStartPosition(Vector3 pos)
        {
            transform.localPosition = pos;
            startPosition = pos;
        }

        public void StartNote()
        {
            startTime = Time.time;
            hasStarted = true;
            hasFinished = false;
        }
        
        public void FinishNote()
        {
            finishTime = Time.time;
            hasStarted = false;
            hasFinished = true;
        }

        private void Start()
        {
            meshFilter = GetComponent<MeshFilter>();
            CollapseQuadLength(meshFilter.mesh);

            void CollapseQuadLength(Mesh mesh)
            {
                quadLength = 0;
                Vector3[] vertices = mesh.vertices;
                for (Int32 i = 0; i < vertices.Length; ++i)
                    vertices[i] = new Vector3(0, vertices[i].y, vertices[i].z);
                mesh.vertices = vertices;
            }
        }

        private void Update()
        {
            Single time = Time.time;
            if (hasStarted)
            {
                if (time < finishTime)
                    AdvanceQuadLength(meshFilter.mesh, speed);
                else 
                    FinishNote();
            }
            else if (hasFinished)
                Move(speed);
            else if (time >= startTime) 
                StartNote();
        }

        private void Move(Single dx)
        {
            Single x = startPosition.x - (Time.time - finishTime) * dx;
            transform.localPosition = new Vector3(x, transform.localPosition.y, transform.localPosition.z);
        }

        private void AdvanceQuadLength(Mesh mesh, Single dx)
        {
            Vector3[] vertices = mesh.vertices;
            quadLength = (Time.time - startTime) * dx;
            vertices[0] = new Vector3(-quadLength, vertices[0].y, vertices[0].z);
            vertices[2] = new Vector3(-quadLength, vertices[2].y, vertices[2].z);
            mesh.vertices = vertices;
        }
    }
}
