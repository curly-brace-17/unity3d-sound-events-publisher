using System;
using System.IO;
using Oak.Dan.Sound;
using UnityEngine;
using Object = System.Object;

namespace Oak.Dan.SoundEventsPublisher.NotesVisualization
{
    public class NotesVisualizer : MonoBehaviour
    {
        [SerializeField] private GameObject notesVisualizationPrefab;
        
        [SerializeField] private String resourceName;

        [SerializeField] private Transform middleCLane;

        [SerializeField] private Vector3 spacingBetweenLanes;


        private SoundPublisher soundPublisher;

        private Single startTime;
        private Boolean started;
        
        private void Start()
        {
            TextAsset midiAsset = Resources.Load<TextAsset>(resourceName);
            MemoryStream midiStream = new MemoryStream(midiAsset.bytes);
            soundPublisher = new SoundPublisher(midiStream);
            soundPublisher.OnNoteStarted += (Object sender, SoundEvent soundEvent) => CreateQuad(soundEvent);
            soundPublisher.OnNoteFinished += (Object sender, SoundEvent soundEvent) => FinalizeQuad(soundEvent);
        }

        private void Update()
        {
            if (!started && Time.time > 1)
            {
                startTime = Time.time;
                soundPublisher.Start();
                started = true;
            }
            else
                soundPublisher.Tick();
        }


        private void FinalizeQuad(SoundEvent soundEvent)
        {
            Debug.Log($"Quad finilized: {soundEvent}");
        }

        private void CreateQuad(SoundEvent soundEvent)
        {
            Int32 n = soundEvent.Pitch.NumberOfSemitonesToMiddleC;
            Vector3 position = middleCLane.localPosition + n * spacingBetweenLanes;
            Quaternion rotation = Quaternion.identity;
            NoteVisualization visualization = Instantiate(notesVisualizationPrefab, position, rotation, transform)
                .GetComponent<NoteVisualization>();
            visualization.SetStartPosition(position);
            visualization.StartTime = startTime + (Single) soundEvent.Time.Value;
            visualization.FinishTime = visualization.StartTime + (Single) soundEvent.Length.Value;
        }
    }
}
