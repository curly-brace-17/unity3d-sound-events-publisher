using System;
using UnityEngine;

namespace Oak.Dan.SoundEventsPublisher.NotesVisualization
{
    public class AudioPlayer : MonoBehaviour
    {
        private AudioSource audioSource;
        private Boolean started;

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }

        private void Update()
        {
            if (!started && Time.time > 1)
            {
                audioSource.Play();
                started = true;
            }
        }
    }
}
