using System;

namespace Oak.Dan.SiUnits
{
    public class Second
    {
        public Double Value { get; }

        public Second(Double value) { Value = value; }

        public override String ToString() { return $"{Value}s"; }
    }
}
