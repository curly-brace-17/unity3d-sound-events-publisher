using System;

namespace Oak.Dan.Music
{
    public class Pitch
    {
        public Octave Octave { get; }

        public Note Note { get; }

        public Int32 NumberOfSemitonesToMiddleC => Note.Count * Octave.Value + this.Note.Index - 48;
        
        public Pitch(Octave octave, Note note)
        {
            Octave = octave;
            Note = note;
        }

        public override String ToString()
        {
            return $"{Note}{Octave}";
        }
    }
}
