using System;

namespace Oak.Dan.Music
{
    // Octave as defined in European music theory
    public class Octave
    {
        public Int32 Value { get; }

        public Octave(Int32 value) { Value = value; }

        public override String ToString() { return Value.ToString(); }
    }
}
