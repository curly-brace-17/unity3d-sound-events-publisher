using System;
using System.Collections.ObjectModel;

namespace Oak.Dan.Music
{
    // Note in pitch class understanding, numerical representation of a pitch in its octave, as defined by European
    // music theory: C = 1, C#/Db = 2, ... 
    public class Note
    {
        public Int32 Value { get; }
        
        public Int32 Index => Value - 1;

        public Note(Int32 value) { Value = value; }

        public override String ToString() { return Names[Index]; }

        public const Int32 Count = 12;

        private static readonly ReadOnlyCollection<String> Names =
            Array.AsReadOnly(new[] {
                "C", "C#/Db", "D", "D#/Eb", "E", "F", "F#/Gb", "G", "G#/Ab", "A", "A#/Bb", "B"
            });
    }
}
