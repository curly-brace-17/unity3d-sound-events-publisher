using System;
using System.Globalization;

namespace Oak.Dan.Sound
{
    public class Velocity
    {
        public Double Value { get; }
        
        public Velocity(Double value) { Value = value; }

        public override String ToString() { return $"{Value.ToString(CultureInfo.InvariantCulture)}v"; }
    }
}
