using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Melanchall.DryWetMidi.Core;
using Melanchall.DryWetMidi.Devices;
using Melanchall.DryWetMidi.Interaction;
using Oak.Dan.Music;
using Oak.Dan.SiUnits;
using UnityEngine;
using DwmNote = Melanchall.DryWetMidi.Interaction.Note;
using Note = Oak.Dan.Music.Note;
using Object = System.Object;

namespace Oak.Dan.Sound
{
    public class SoundPublisher
    {
        public event EventHandler<SoundEvent> OnNoteStarted;
        
        public event EventHandler<SoundEvent> OnNoteFinished;
        
        public Boolean IsRunning => dwmPlayback.IsRunning;

        public SoundPublisher(Stream bytesStream)
        {
            MidiFile dwmMidiFile = MidiFile.Read(bytesStream);
            dwmTempoMap = dwmMidiFile.GetTempoMap();
            dwmNotes = dwmMidiFile.GetNotes().ToList();
            dwmPlayback = dwmMidiFile.GetPlayback(ManualPlaybackSettings);

            dwmPlayback.NotesPlaybackStarted += (Object sender, NotesEventArgs args) => 
                OnNoteStarted?.Invoke(this, GetFirstSoundEvent(args));
            dwmPlayback.NotesPlaybackFinished += (Object sender, NotesEventArgs args) => 
                OnNoteFinished?.Invoke(this, GetFirstSoundEvent(args));
            dwmPlayback.Finished += (sender, args) => dwmPlayback.Dispose();
        }
        
        public void Start()
        {
            dwmPlayback.Start();
        }
        
        public void Tick()
        {
            if (dwmPlayback.IsRunning) dwmPlayback.TickClock();
        }

        private SoundEvent GetFirstSoundEvent(NotesEventArgs args)
        {
            DwmNote note = args.Notes.FirstOrDefault(n => n.Channel == 0);
            SoundEvent soundEvent = ToSoundEvent(note);
            return soundEvent;
        }
        
        private List<DwmNote> dwmNotes;
        
        private TempoMap dwmTempoMap;

        private Playback dwmPlayback;
        
        // Playback settings used for manual ticking.
        // https://melanchall.github.io/drywetmidi/articles/playback/Tick-generator.html#manual-ticking
        private static PlaybackSettings ManualPlaybackSettings =
            new PlaybackSettings
            {
                ClockSettings = new MidiClockSettings {CreateTickGeneratorCallback = () => null}
            };
        
        private SoundEvent ToSoundEvent(DwmNote dwmNote)
        {
            return new SoundEvent(
                null,
                new Pitch(
                    new Octave(dwmNote.Octave),
                    new Note((dwmNote.NoteNumber - 60) % 12 + 1)),
                new Velocity(dwmNote.Velocity / 127.0), 
                new Second(dwmNote.TimeAs<MetricTimeSpan>(dwmTempoMap).TotalMicroseconds / 1000000d),
                new Second(dwmNote.LengthAs<MetricTimeSpan>(dwmTempoMap).TotalMicroseconds / 1000000d));
        }
    }
}
