using System;

namespace Oak.Dan.Sound
{
    public class Id
    {
        public Int32 Value { get; }

        public Id(Int32 value) { Value = value; }

        public override String ToString() { return $"{Value}"; }
    }
}
