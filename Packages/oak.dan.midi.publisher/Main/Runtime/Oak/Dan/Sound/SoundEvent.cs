using System;
using Oak.Dan.Music;
using Oak.Dan.SiUnits;

namespace Oak.Dan.Sound
{
    public class SoundEvent
    {
        public Id Id { get;  }
        
        public Pitch Pitch { get; }

        public Velocity Velocity { get; }

        public Second Time { get; }
        
        public Second Length { get; }
        
        public SoundEvent(Id id, Pitch pitch, Velocity velocity, Second time, Second length)
        {
            Id = id;
            Pitch = pitch;
            Velocity = velocity;
            Time = time;
            Length = length;
        }

        public override String ToString() => $"{Pitch} {Velocity} {Length}";
    }
}
