using NUnit.Framework;

namespace Oak.Dan.Music
{
    public class NoteTest
    {
        [Test]
        public void ToStringTest()
        {
            Note dSharp = new Note(4);
            Assert.AreEqual(expected: "D#/Eb", actual: dSharp.ToString());
        }
    }
}
