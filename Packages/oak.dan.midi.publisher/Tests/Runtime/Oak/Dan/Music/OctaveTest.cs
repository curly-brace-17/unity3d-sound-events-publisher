using NUnit.Framework;

namespace Oak.Dan.Music
{
    public class OctaveTest
    {
        [Test]
        public void ToStringTest()
        {
            Octave middleOctave = new Octave(4);
            Assert.AreEqual(expected: "4", actual: middleOctave.ToString());
        }
    }
}
