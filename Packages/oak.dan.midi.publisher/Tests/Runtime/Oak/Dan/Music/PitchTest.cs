using NUnit.Framework;

namespace Oak.Dan.Music
{
    public class PitchTest
    {
        [Test]
        public void ToStringTest()
        {
            Pitch middleCSharp = new Pitch(new Octave(4), new Note(2));
            Assert.AreEqual(expected: "C#/Db4", actual: middleCSharp.ToString());
        }
    }
}
