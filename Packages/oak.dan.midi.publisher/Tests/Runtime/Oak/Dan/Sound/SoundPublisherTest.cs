using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Oak.Dan.Sound;
using UnityEngine;
using Object = System.Object;

namespace Tests.Runtime.Oak.Dan.Sound
{
    public class SoundPublisherTest
    {
        [Test]
        public void SingleSoundEventTest()
        {
            List<SoundEvent> soundEvents = new List<SoundEvent>();
            MemoryStream midiBytesStream = LoadMidiBytesStream("single_middle_c.mid");

            SoundPublisher soundPublisher = new SoundPublisher(midiBytesStream);
            soundPublisher.OnNoteStarted += (Object sender, SoundEvent soundEvent) => soundEvents.Add(soundEvent);

            soundPublisher.Start();
            while (soundPublisher.IsRunning) soundPublisher.Tick();
            
            Assert.AreEqual(1, soundEvents.Count);
            Assert.AreEqual(4, soundEvents[0].Pitch.Octave.Value);
            Assert.AreEqual(1, soundEvents[0].Pitch.Note.Value);
            Assert.AreEqual(1d, soundEvents[0].Velocity.Value);
        }
        
        [Test]
        public void NokiaTuneTest()
        {
            List<SoundEvent> soundEvents = new List<SoundEvent>();
            MemoryStream midiBytesStream = LoadMidiBytesStream("nokia_gran_vals_tune.mid");

            SoundPublisher soundPublisher = new SoundPublisher(midiBytesStream);
            soundPublisher.OnNoteStarted += (Object sender, SoundEvent soundEvent) => soundEvents.Add(soundEvent);

            soundPublisher.Start();
            while (soundPublisher.IsRunning) soundPublisher.Tick();
            
            Assert.AreEqual(13, soundEvents.Count);

            String[] names = 
                {"E5", "D5", "F#/Gb4", "G#/Ab4", "C#/Db5", "B4", "D4", "E4", "B4", "A4", "C#/Db4", "E4", "A4"};
            Double[] lengths = 
                {1/8d, 1/8d, 1/4d, 1/4d, 1/8d, 1/8d, 1/4d, 1/4d, 1/8d, 1/8d, 1/4d, 1/4d, 1/2d};
            Double tempo = 220d / 60 / 4; // 220 beats per minute for a quarter note

            for (Int32 i = 0; i < soundEvents.Count; ++i)
            {
                Assert.AreEqual(names[i], soundEvents[i].Pitch.ToString());
                Assert.That(soundEvents[i].Length.Value, Is.EqualTo(tempo * lengths[i]).Within(0.0001));
            }
        }

        private static MemoryStream LoadMidiBytesStream(String resourcePath) =>
            new MemoryStream(Resources.Load<TextAsset>(resourcePath).bytes);
    }
}
