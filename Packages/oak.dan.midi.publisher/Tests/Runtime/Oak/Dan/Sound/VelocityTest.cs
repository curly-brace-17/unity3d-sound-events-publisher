using NUnit.Framework;
using Oak.Dan.Sound;

namespace Tests.Runtime.Oak.Dan.Sound
{
    public class VelocityTest
    {
        [Test]
        public void ToStringTest()
        {
            Velocity velocity = new Velocity(0.5);
            Assert.AreEqual(expected: "0.5v", actual: velocity.ToString());
        }
    }
}
