using NUnit.Framework;
using Oak.Dan.Music;
using Oak.Dan.SiUnits;
using Oak.Dan.Sound;

namespace Tests.Runtime.Oak.Dan.Sound
{
    public class SoundEventTest
    {
        [Test]
        public void ToStringTest()
        {
            SoundEvent loudMiddleC = new SoundEvent(
                new Id(1),
                new Pitch(
                    new Octave(4),
                    new Note(1)),
                new Velocity(1), null, new Second(1));
            Assert.AreEqual(expected: "C4 1v 1s", actual: loudMiddleC.ToString());
        }
    }
}
